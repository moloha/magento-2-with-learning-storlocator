let config = {
    "map": {
        "*": {
            mageReviewExample: "Magento_Theme/js/mageReviewExample",
            mageReviewExampleNew: "Magento_Theme/js/mageReviewExampleNew",
            mageModalTask: "Magento_Theme/js/mageModalTask",
            mageLoaderWidget: "Magento_Theme/js/mageLoaderWidget"
        }
    },
    config: {
        mixins: {
            'Magento_Theme/js/mageReviewExample': {
                'Magento_Theme/js/mageReviewExample-mixin': true
            }
        }
    }
};
