define([
    "jquery",
    "Magento_Ui/js/modal/modal"
],function($, modal) {

    let options = {
        type: 'popup',
        responsive: true,
        title: 'Reviews========',
        buttons:false
        //     [{
        //     text: $.mage.__('Ok'),
        //     class: '',
        //     click: function () {
        //         this.closeModal();
        //     }
        // }]
    };

    let popup = modal(options, $('#modal-content-3'));
    $("#modal-btn").click(function() {
        $('#modal-content-3').modal('openModal');
    });
});
