define(['jquery'], function ($) {
    'use strict';

    let mageReviewMixin = {

        /**
         * Added confirming for modal closing
         *
         * @returns {Element}
         */
        _create: function () {
            console.log('Mixin ready!????!!!444');
            return this._super();
        }
    };

    return function (targetWidget) {
        // Example how to extend a widget by mixin object
        $.widget('Ideainyou.mageReviewExample', targetWidget, mageReviewMixin); // the widget alias should be like for the target widget

        return $.Ideainyou.mageReviewExample; //  the widget by parent alias should be returned
    };
});
