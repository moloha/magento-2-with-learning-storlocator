define(['jquery', 'loader', 'domReady!'], function ($) {
    'use strict';

    $('body').trigger('processStart');
    setTimeout(function (){
        $('body').trigger('processStop');
    },2000 );
});
