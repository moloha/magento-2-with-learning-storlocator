<?php


namespace IdeaInYou\Review\Api;

interface StoreInterface
{
    const TABLE_NAME = 'ideainyou_stores';
    const ENTITY_ID = 'entity_id';
    const COUNTRY = 'country';
    const CITY = 'city';
    const STORE = 'store';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';

    public function getId();

    public function setId($id);

    public function getCountry();

    public function setCountry($country);

    public function getCity();

    public function setCity($city);

    public function getStore();

    public function setStore($store);

    public function getLatitude();

    public function setLatitude($latitude);

    public function getLongitude();

    public function setLongitude($longitude);

}
