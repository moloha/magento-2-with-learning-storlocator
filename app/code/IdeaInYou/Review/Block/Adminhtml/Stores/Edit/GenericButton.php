<?php
namespace IdeaInYou\Review\Block\Adminhtml\Stores\Edit;

use Magento\Backend\Block\Widget\Context;
use IdeaInYou\Review\Api\StoreRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;
    private StoreRepositoryInterface $storeRepository;


    /**
     * @param Context $context
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository
    ) {

        $this->context = $context;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Return CMS block ID
     *
     * @return int|null
     */
    public function getStoreId()
    {
        try {
            return $this->storeRepository->getById(
                    $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
