<?php

namespace IdeaInYou\Review\Controller\Adminhtml\Stores;

use IdeaInYou\Review\Api\StoreRepositoryInterface;
use IdeaInYou\Review\Model\StoreFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Edit CMS block action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    protected PageFactory $resultPageFactory;

    private StoreFactory $storeFactory;
    private StoreRepositoryInterface $storeRepository;
    private Registry $_coreRegistry;


    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        StoreFactory $storeFactory,
        StoreRepositoryInterface $storeRepository
    ) {



        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
        $this->storeFactory = $storeFactory;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Edit CMS block
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');



        // 2. Initial checking
        if ($id) {
            try {
                $model = $this->storeRepository->getById($id);
            } catch (NoSuchEntityException $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $model = $this->storeFactory->create();
        }

        $this->_coreRegistry->register('ideainyou_stores', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Store: %1', $model->getId()) : __('New Store'));
        return $resultPage;
    }
}
