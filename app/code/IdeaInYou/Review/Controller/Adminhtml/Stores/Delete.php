<?php
namespace IdeaInYou\Review\Controller\Adminhtml\Stores;

use IdeaInYou\Review\Api\StoreRepositoryInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;

/**
 * Class MassDelete
 */
class Delete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{



    private StoreRepositoryInterface $storeRepository;


    /**
     * @param Context $context
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository
    ) {
        parent::__construct($context);
        $this->storeRepository = $storeRepository;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {

        $storeId = $this->getRequest()->getParam('id');

        if ($storeId){
            try {
                $this->storeRepository->deleteById($storeId);
                $this->messageManager->addSuccessMessage(__('Store (ID: %1) have been deleted.', $storeId));
            } catch (\Exception $exception){
                $this->messageManager->addErrorMessage($exception->getMessage());
            }

        }


        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
