<?php


namespace IdeaInYou\Review\Controller\Adminhtml\Stores;

use IdeaInYou\Review\Api\StoreRepositoryInterface;
use IdeaInYou\Review\Model\StoreFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Registry;

/**
 * Save CMS block action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    private Registry $_coreRegistry;
    /**
     * @var StoreFactory|mixed
     */
    private $storeFactory;
    /**
     * @var StoreRepositoryInterface|mixed
     */
    private $storeRepository;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DataPersistorInterface $dataPersistor,
        StoreFactory $storeFactory = null,
        StoreRepositoryInterface $storeRepository = null
    )
    {
        $this->dataPersistor = $dataPersistor;
        $this->storeFactory = $storeFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StoreFactory::class);
        $this->storeRepository = $storeRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StoreRepositoryInterface::class);
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            //проверяю наличие longitude latitude
            if (empty($data['latitude']) || empty($data['longitude'])){

                try {
                    //Address,

                    $zip = $data['zip'];
                    $str_address = $data['street_address'];
                    $city = $data['city'];
                    $country = $data['country'];
                    $address = strval($zip).' '.$str_address.' '.$city.' '.$country;
                    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=YOUR_API_KEY');
                    $coordinates = json_decode($coordinates);
                    $data['latitude'] = $coordinates->results[0]->geometry->location->lat;
                    $data['longitude'] = $coordinates->results[0]->geometry->location->lng;
                }catch (\Exception $e){
                    $this->messageManager->addErrorMessage($e->getMessage());
                }
            }


            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->storeFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model = $this->storeRepository->getById($id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('This store no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->storeRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the store.'));
                $this->dataPersistor->clear('ideainyou_stores');
                return $this->processBlockReturn($model, $data, $resultRedirect);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }

            $this->dataPersistor->set('ideainyou_stores', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the block return
     *
     * @param \Magento\Cms\Model\Block $model
     * @param array $data
     * @param \Magento\Framework\Controller\ResultInterface $resultRedirect
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processBlockReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect === 'continue') {
            $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        }
        return $resultRedirect;
    }


}
