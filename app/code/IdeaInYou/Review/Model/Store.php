<?php


namespace IdeaInYou\Review\Model;

use IdeaInYou\Review\Api\StoreInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Store extends \Magento\Framework\Model\AbstractModel implements StoreInterface, IdentityInterface
{
    const  CACHE_TAG = 'ideainyou_stores';

    protected $_eventPrefix = 'ideainyou_stores';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\IdeaInYou\Review\Model\ResourceModel\Store::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }



    public function getCountry()
    {
        return parent::getData(self::COUNTRY);
    }

    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    public function getCity()
    {
        return parent::getData(self::CITY);
    }

    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    public function getStore()
    {
        return parent::getData(self::STORE);
    }

    public function setStore($store)
    {
        return $this->setData(self::STORE,$store);
    }

    public function getLatitude()
    {
        return parent::getData(self::LATITUDE);
    }

    public function setLatitude($latitude)
    {
        return $this->setData(self::LATITUDE,$latitude);
    }

    public function getLongitude()
    {
        return parent::getData(self::LONGITUDE);
    }

    public function setLongitude($longitude)
    {
        return $this->setData(self::LONGITUDE,$longitude);
    }
}
