<?php


namespace IdeaInYou\Review\Model\ResourceModel\Store;

use IdeaInYou\Review\Model\Store;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Store::class,
            \IdeaInYou\Review\Model\ResourceModel\Store::class
        );
    }


//    public function asd()
//    {
//        $this->addFieldToFilter('entity_id', ['neq'=>'2']);
//        $this->getData();
//    }


}
