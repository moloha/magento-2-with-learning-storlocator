<?php

namespace IdeaInYou\Review\Model\ResourceModel;

use IdeaInYou\Review\Api\StoreInterface;

class Store extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(StoreInterface::TABLE_NAME, StoreInterface::ENTITY_ID);
    }
}
