<?php

namespace IdeaInYou\Review\Model;

use IdeaInYou\Review\Api\StoreRepositoryInterface;
use IdeaInYou\Review\Model\ResourceModel\Store as ResourceStore;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class StoreRepository implements StoreRepositoryInterface
{
    private StoreFactory $storeFactory;
    private ResourceStore $resource;

    /**
     * @param StoreFactory $storeFactory
     * @param ResourceStore $resource
     */
    public function __construct(
        StoreFactory $storeFactory,
        ResourceStore $resource
    ) {

        $this->storeFactory = $storeFactory;
        $this->resource = $resource;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getById($id): Store
    {
        $store = $this->storeFactory->create();
        $this->resource->load($store, $id);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('Store with id "%1" does not exist.', $id));
        }
        return $store;
    }

    /**
     * @throws CouldNotSaveException
     */
    public function save(Store $store): Store
    {
        try {
            $this->resource->save($store);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Store: %1',
                $exception->getMessage()
            ));
        }
        return $store;
    }

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(Store $store): bool
    {
        try {
            $this->resource->delete($store);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Store: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param $id
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $this->delete($this->getById($id));
    }

}
