define([
    "jquery",
    "Magento_Ui/js/modal/modal"
],function($, modal) {

    let options = {
        type: 'popup',
        responsive: true,
        title: 'Modal Reviews',
        buttons: [{
            text: $.mage.__('Ok'),
            class: '',
            click: function () {
                this.closeModal();
            }
        }]
    };

    let popup = modal(options, $('#modal-content'));
    $("#modal-btn1").click(function() {
        $('#modal-content').modal('openModal');
    });
});
